([{
    tech: 'js',
    mustDeps: [
      {tech: 'bemhtml', block: 'svg'},
    ]
  },
  {
    mustDeps: [
      {block: 'modernizr'},
      {block: 'snapsvg'},
    ],
    shouldDeps: [],
}])
