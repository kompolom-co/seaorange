/* global modules:false */

modules.define('gradt', ['modernizr','bh', 'i-bem__dom'],
               function(provide, Modernizr, BH, BEMDOM) {

provide(BEMDOM.decl('gradt', {
  onSetMod: {
    'js': {
      'inited': function(){
        if (Modernizr.inlinesvg) {
//          this.setMod('svg', true);
        }
      }
    },
    'svg' : {
      'true': function(){
         this.text = this.domElem.html();
         BEMDOM.update(this.domElem, BH.apply({
           block: 'svg',
           attrs: {
            width:  this.domElem.width(),
            height: this.domElem.height()
           },
          content:[
             {
                tag: 'defs',
                content: [
                  {
                    tag: 'linearGradient',
                    arrts: { id: 'tg', x1:"0%", x2:"0%",  y1:"0%", y2:"100%" },
                    content: '',
                  }
                ]
             },
             {
              elem: 'text',
              tag: 'text',
              attrs: {
                fill: this.domElem.css('color'),
                width: '100%',
                x: 0,
                y: this.domElem.height() * 0.7
              },
              content: this.text
             }
           ]
         }));
      }
    }
  }
}));

});

