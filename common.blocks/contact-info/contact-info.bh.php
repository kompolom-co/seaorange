<?php
return function ($bh) {
 $bh->match("contact-info", function ($ctx, $json){
   $ctx->content(
     [
       'elem' => 'wrap',
       'content' => $ctx->content()
     ], true
   );
 });
};
