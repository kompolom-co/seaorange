module.exports = function (bh) {
 bh.match("callme", function (ctx, json){
   ctx.content([
      {
        'elem' : 'text',
        'content' : ctx.param('text')
      },
      {
        'block': 'phone',
        'mix' : [
          {'block':'callme', 'elem': 'phone'},
          {'block':'gradt', 'mods':{'cs':'yellow'}},
        ],
        'content': ctx.param('phone')
      }
   ]);
 });
};
