/* global modules:false */

modules.define('scrollscreen', ['jquery','i-bem__dom'], function(provide, $, BEMDOM) {

provide(BEMDOM.decl('scrollscreen',{
  onSetMod: {
    'js': {
      'inited' : function(){
        this.speed = this.params.speed || 600;
        this.easing = this.params.easing || 'swing';
        this.ss = this.findBlockOn('scrollspy');
        this.screenH = BEMDOM.win.height(); //высота экрана
        this.ss.setOffset(0);
        this.ss.on('scrollin', this._onScrollIn, this);

        this.multiscreen = this.domElem.height() > this.screenH;
      },
      '': function(){
        this.ss.delMod('js');
      }
    }
  },

  _onScrollIn: function(e, dir){
      if (dir === "down" && !this.scrollnow) {
        this.goTop();
      }else if (dir === "up" && this.isTop()) {
        return;
      }else if (dir === "up" && !this.isTop() && this.multiscreen) {
        this.goBottom();
      }else if (dir === "up" && !this.isTop() && !this.multiscreen) {
        this.goTop();
      }
  },

  goNext: function(){
    //console.log('go next');
    this._animate(this.domElem.offset().top + this.screenH); 
  },

  goBottom: function(){
    //console.log('go bottom');
    this._animate(this.domElem.offset().top + this.domElem.height() - this.screenH); 
    //this.unbindFrom(BEMDOM.win, 'scroll');
    //console.log('unbind: bottom');
  },

  goTop: function(){
    //console.log('go top');
    this._animate(this.domElem.offset().top); 
  },

  isTop: function(){
    t = this.domElem.offset().top;
    p = BEMDOM.win.scrollTop();
    return t >= p;
  },

  _animate: function(pos){
    $('html, body').animate({scrollTop: pos}, this.speed, this.easing);
  },
}));

});

