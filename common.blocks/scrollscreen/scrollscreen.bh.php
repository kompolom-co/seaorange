<?php
return function ($bh) {
 $bh->match("scrollscreen", function ($ctx, $json){
   $ctx
     ->js(true)
     ->mix([
     ['block' => 'scrollspy', 'js'=>['offset' => '0']],
     ['block' => 'screen-h', 'mods'=>['soft' => true], 'js'=>true],
   ]);
 });
};
