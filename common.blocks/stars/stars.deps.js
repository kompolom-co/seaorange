([
  {
    tech: 'js',
    mustDeps:[
      {tech: 'bemhtml', block: 'icon', mods: {star: true}}
    ]
  },
  {
    mustDeps: [],
    shouldDeps: [
      {block: 'icon', mods: {star: ['empty', 'full']}}
    ]
}])
