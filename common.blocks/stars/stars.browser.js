/* global modules:false */

modules.define('stars', ['bh','i-bem__dom'], function(provide, BH, BEMDOM) {

provide(BEMDOM.decl('stars', {
  'onSetMod': {
    'js': {
       'inited': function() {
          this.count = this.params.count || 0;
          this.max = this.params.max || 5;
          this._renderStars();
       }
    }
  },
  _renderStars: function(){
    var stars = [];
    for (var i = 0; i < this.max; i++) {
      stars.push(
          {
            block: 'icon',
            mods: {star: i < this.count? 'full': 'empty'},
          }
      );
    }
    BEMDOM.update(this.domElem, BH.apply(stars));
  }
}));

});

