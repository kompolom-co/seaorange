<?php
return function ($bh) {
 $bh->match("stars", function ($ctx, $json){
   $ctx->js(['count'=> $ctx->param('count'), 'max'=> $ctx->param('max')]);
 });
};
