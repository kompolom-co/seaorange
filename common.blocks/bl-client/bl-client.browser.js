/* global modules:false */

modules.define('bl-client',
               ['jquery', 'vow'],
               function(provide, $, vow) {

provide({
  apiUrl: '/ajax.php',

  /**
   * Load Block content
   * @param block {string} blocks group name or block name
   * @param blockid {int} block offset
   * @returns {object} promise
   */
  getJson: function(block, blockid) {
    var deferred = vow.defer();
     $.ajax({
        url: this.apiUrl,
        dataType: 'json',
        data: {
          b: block,
          id: blockid,
          json: true
        },
        success: function(res){
          //TODO: test is object
          deferred.resolve(res, this);
        },
        context: this
     });
    return deferred.promise();
  }
}); 
});

