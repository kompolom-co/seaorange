<?php
return function ($bh) {
 $bh->match("thankyou", function ($ctx, $json){
   $ctx
     ->mix(['block' => 'container'])
     ->content([
      [
          [
              [
                block=> "row",
                mix => ['block'=>'thankyou', 'elem' => 'header'],
                mods=> [svam=> true],
                content=> [
                  [
                    elem=> 'col',
                    mods=> [mw=> 4],
                    content=> [
                      [
                        block=> 'logo',
                        'url' => $ctx->param('logo'),
                        'name' => $ctx->param('logoname'),
                        'desc' => $ctx->param('logodesc'),
                      ]
                    ]
                  ],
                  [
                    elem=> 'col',
                    mods=> [mw=> 4],
                    content=> [
                      [
                        block=> 'sitename',
                        content=> $ctx->param("sitename")
                      ]
                    ]
                  ],
                  [
                    elem=> 'col',
                    mods=> [mw=> 4],
                    mix => [
                      'block' => 'row',
                      mods => ['svam' => true, 'sac'=>true]
                    ],
                    content=> [
                      [
                        block=> 'callme',
                        mix => [block => 'row', elem => 'col', mods => [mw => 10]],
                        phone=> $ctx->param('phone')
                      ],
                    ]
                  ],
                ]
              ], //row
              [
                block => 'row',
                content => [
                  [
                    elem => 'col',
                    mods => [mw=>8],
                    content => [
                      [
                        block => 'thankyou',
                        elem => 'content',
                        content => [
                          [
                            elem => 'text',
                            content => [
                              [
                                elem => 'user',
                                content => $ctx->param('user').','
                              ],
                              'Агенство путешествий Оранжевое море благодарит вас за обращение<br><br>',
                              [
                                elem => 'wait',
                                content =>'Мы свяжемся с Вами в ближайшее время.<br>',
                              ],
                              'С уважением, Ваш личный менеджер ',
                              [
                                elem => 'manager-name',
                                content => $ctx->param('manager')
                              ]
                            ]
                          ]
                        ]
                      ],
                      [
                        block => 'thankyou',
                        elem => 'close',
                        content => [
                          [
                            block => 'button',
                            mix => [block=> 'modal', 'elem' => 'close'],
                            mods => ['theme' => 'grad', 'size' => 'm'],
                            text => 'Закрыть окно',
                          ]
                        ]
                      ]
                    ]
                  ],
                  [
                    elem => 'col',
                    mods => [mw => 4],
                    content => [
                      [
                        block => 'image',
                        mix => [block=>'thankyou', 'elem'=>'manager'],
                        url => $ctx->param('managerUrl'),
                      ],
                    ]
                  ]
                ]
              ], //row
          ]
        ]
   ]);
 });
};
