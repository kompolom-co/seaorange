([
  {
    tech: 'js',
    mustDeps: [
      {tech: 'bemhtml', block: 'logo'},
      {tech: 'bemhtml', block: 'callme'},
    ]
  },
  {
    mustDeps: [
      {block: 'row'},
      {block: 'sitename'},
      {block: 'callme'},
      {block: 'container'},
    ],
    shouldDeps: [
      {block: 'logo'},
      {block: 'button', mods: {theme: 'grad', size: 'm' } },
    ]
}
])
