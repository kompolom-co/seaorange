<?php
return function ($bh) {
 $bh->match("price", function ($ctx, $json){
   $ctx
     ->js(true)
     ->content([
      [
        'elem'=> 'from',
        'tag' => 'span',
        'content' => $ctx->param('from')
      ],
      [
        'elem' => 'count',
        'tag' => 'span',
        'content' => $ctx->param('count')
      ],
      [
        'elem' => 'curr',
        'tag' => 'span',
        'content' => $ctx->param('curr')
      ]
   ]);
 });
};
