([{
    tech: 'js',
    shouldDeps: [
      {tech: 'bemhtml', block: 'modal', mods: {autoclosable: true, theme: 'islands'}},
      {tech: 'bemhtml', block: 'thankyou'},
    ]
  },
  {
    mustDeps: [],
    shouldDeps: [
      {block: 'form', mods: {async: true}},
      {block: 'modal', mods: {autoclosable: true, theme: 'islands'}},
      {block: 'thankyou'}
    ]
}])
