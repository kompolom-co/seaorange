/* global modules:false */

modules.define('form_target_modal',
               ['form_async', 'BEMHTML', 'i-bem__dom'],
               function(provide, Form, BEMHTML, BEMDOM) {

provide(BEMDOM.decl({block: 'form', modName:'target', modVal: 'modal'},{
  'onSetMod' : {
    'js': {
      'inited': function(){
        debugger
        this.__base.apply(this, arguments);
        this.on('success', this.showModal, this);
      }
    }
  },

  /**
   * return modal bemjson
   */
  _getTemplate: function(){
    return {
      block: 'modal',
      mods: {autocloseable: true, theme: 'islands'},
      content: this.res.msg
    };
  },


  /**
   * Показывает модальное окно
   */
  showModal: function(e, res) {
    this.res = res;
    modal = this.renderModal();
    this._modal.setMod('visible', true);
  },

  /**
   * Render Modal
   * @returns {object} modal
   */
  renderModal: function(){
    var modal = BEMDOM.append(this.domElem, BEMHTML.apply(this._getTemplate()));
    this._modal = this.findBlockOn(modal, 'modal');
    return this._modal;
  }
}));

});

