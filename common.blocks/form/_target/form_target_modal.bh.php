<?php
return function ($bh) {
 $bh->match("form_target_modal", function ($ctx, $json){
   $ctx->mods(['async' => true]);
 });
};
