({
    mustDeps: [],
    shouldDeps: [
      {block: 'hidden'},
      {block: 'form', mods: {async: 'true', target: 'modal'}},
      {block: 'input', mods: {type:['hidden','tel','textarea']}},
      {block: 'lightbox'},
      {block: 'respond'}
    ]
})
