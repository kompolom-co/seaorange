<?php
return function ($bh) {
 $bh->match("copyright", function ($ctx, $json){
   $ctx->content([
     [
       'elem' => 'ico',
     ],
     [
       'elem' => 'text',
       'content' => $ctx->content()
     ]
   ], true);
 });
};
