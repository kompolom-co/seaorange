module.exports = function(bh) {

  bh.match('svg', function(ctx, json) {
    ctx
      .tag('svg')
      .attrs({
        xmlns:'http://www.w3.org/2000/svg',
      });
  });
}
