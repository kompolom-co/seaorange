
module.exports = function(bh) {
  bh.match('list', function(ctx){
    ctx.tag('ul');
  });
  bh.match('list__item', function(ctx){
    ctx.tag('li');
  });
}
