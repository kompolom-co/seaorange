<?php
return function ($bh) {
 $bh->match("list", function ($ctx, $json){
   $ctx->tag('ul');
 });
 $bh->match("list__item", function ($ctx, $json){
   $ctx->tag('li');
 });
};
