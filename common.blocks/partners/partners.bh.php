<?php
return function ($bh) {
 $bh->match("partners", function ($ctx, $json){
   $ctx->content([
     array_map(function($partner){
        return [
          'block' => 'image',
          'url'  => $partner,
          'mix' => ['block' => 'partner']
        ];
     }, $json->partners)
   ], true);
 });
};
