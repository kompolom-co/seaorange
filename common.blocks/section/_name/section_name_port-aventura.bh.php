<?php
return function ($bh) {
 $bh->match("section_name_port-aventura", function ($ctx, $json){
   $ctx
     ->mix(['block' => 'screen-h', 'js' => true])
     ->attr('style', 'background-image: url('.$ctx->param('background').')');
 });
};
