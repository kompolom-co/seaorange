<?php
return function ($bh) {
 $bh->match("section", function ($ctx, $json){
   if ($json->head) {
     $ctx->content([
        [
          'block' => 'heading',
          'lvl' => 2,
          'content' => $ctx->param('head')
        ],
        $ctx->content()
     ],true);
   }
 });
};
