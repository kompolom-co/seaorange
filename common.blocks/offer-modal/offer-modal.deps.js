([{
    tech: 'js',
    mustDeps: [],
    shouldDeps: [
      {tech: 'bemhtml', block: 'form', mods: {async: true}},
      {tech: 'bemhtml', block: 'input', mods: {type: ['hidden', 'tel'], required: true}},
      {tech: 'bemhtml', block: 'input', elem: 'control'},
      {tech: 'bemhtml', block: 'image'},
      {tech: 'bemhtml', block: 'list'},
      {tech: 'bemhtml', block: 'slider', mods: {type: 'owl', pos: 'aventura'}},
      {tech: 'bemhtml', block: 'row'},
      {tech: 'bemhtml', block: 'offer-modal'},
      {tech: 'bemhtml', block: 'modal'},
      {tech: 'bemhtml', block: 'button'},
      {tech: 'bemhtml', block: 'link', mods: {action: 'lightbox'}},
      {block: 'row', elem: 'col'},
    ]
},
{
  tech: 'bemhtml',
  mustDeps: [
    {block: 'offer-modal'}
  ]
},
{
    mustDeps: [
      {block: 'modal'},
    ],
    shouldDeps: [
      {block: 'form'},
      {block: 'vow'},
      {block: 'image'},
      {block: 'slider', mods: {type: ['owl', 'bx'], pos: 'aventura', size: 'm'}},
      {block: 'row'},
      {block: 'link' },
      {block: 'gallery' },
      {block: 'row', elem: 'col'},
    ]
}
])
