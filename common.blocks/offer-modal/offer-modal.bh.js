
module.exports = function(bh) {
  bh.match('offer-modal__content', function(ctx, json) {
    ctx.tag('offer');
    ctx.content([
        {
          block: 'button',
          mods: {theme: 'white'},
          mix: {block: 'offer-modal', elem: 'close'},
          text: 'Закрыть'
        }, 
        {
          elem: 'wrap',
          mix: {block: 'container', mods: {size: 'l'}},
          content: ctx.content()
        }
    ], true);
  });
}
