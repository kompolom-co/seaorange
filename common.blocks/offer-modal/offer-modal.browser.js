/* global modules:false */

modules.define('offer-modal', ['i-bem__dom'],
               function(provide, BEMDOM) {

provide(BEMDOM.decl('offer-modal',{
  onSetMod: {
    js: {
      'inited': function(){
      }
    }
  },
  close: function(){
    this.findBlockOn('modal').setMod('visible', false);
  }
},{
  live: function(){
    this.liveBindTo('close', 'click', function(){
      this.close();
    });
    return false
  }
}));

});

