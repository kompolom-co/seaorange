<?php
return function ($bh) {
 $bh->match("port-aventura__section", function ($ctx, $json){
   $ctx
     ->mix([
       //['block' => 'screen-h', 'js' => true],
       ['block' => 'section', 'mods' => $ctx->mods()],
     ])
     ->content([
       [
         'block' => 'container',
         'mods' => ['size' => 'l'],
         'content' => $ctx->content()
       ]
     ], true)
     ->attr('style', 'background-image: url('.$ctx->param('background').')');
 });
};
