({
    mustDeps: [],
    shouldDeps: [
      {block: 'section'},
      {block: 'container', mods: {size: 'l'}}
    ]
})
