/* global modules:false */

modules.define('slider_type_bx', 
               ['bxslider', 'i-bem__dom', 'slider'],
               function(provide, $, BEMDOM, Slider) {

provide(Slider.decl({block:'slider', modName: 'type', modVal: 'bx'}, {
    onSetMod : {
        'js' : {
            'inited' : function() {                
                this.__base.apply(this, arguments);
                this.slider = $(this.domElem).bxSlider(this.config);
            }
        },

    },

    reload: function(){
      this.slider.reloadSlider();
    }

}));

});

