({
    mustDeps: [],
    shouldDeps: [
      {block: 'jquery', elem: 'owlcarousel'}
    ],
    noDeps: [
      {block: 'bxslider'}
    ]
})
