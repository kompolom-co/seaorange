/* global modules:false */

modules.define('slider_type_owl', ['jquery__owlcarousel', 'i-bem__dom', 'slider'], function(provide, $, BEMDOM, Slider) {

provide(Slider.decl({block:'slider', modName: 'type', modVal: 'owl'}, {
    onSetMod : {
        'js' : {
            'inited' : function() {                
                this.config = this.params;
                $(this.domElem).owlCarousel(this.config);
            }
        },

    },
}));

});

