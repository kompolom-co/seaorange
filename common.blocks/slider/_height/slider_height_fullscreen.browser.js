/* global modules:false */

modules.define('slider_height_fullscreen',
               ['jquery', 'i-bem__dom', 'slider'],
               function(provide, $, BEMDOM, Slider) {

provide(Slider.decl({modName: 'height', modVal: 'fullscreen'},{
  onSetMod: {
    'js': {
      'inited': function(){
        this.__base.apply(this, arguments);                
        //this._offset = this.params.offset || this._calcOffset();
        //this.findBlockOn('screen-h').setOffset(this._offset);
      }
    }
  },
  _calcOffset: function(){
    return this.domElem.offset().top;
  }
}));

});

