
module.exports = function(bh) {

    bh.match('slider_pos_aventura', function(ctx, json) {
      ctx.js({
        items: 1,
        merge: false,
        autoHeight: true,
        loop: true,
        autoplay: true,
        dots: false,
      })
    });

};
