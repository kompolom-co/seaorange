<?php
return function ($bh) {
 $bh->match("slider_pos_aventura", function ($ctx, $json){
   $ctx->js([
     items=> 1,
     center=> true,
     autoHeight => true,
     //animateOut => 'fadeOut',
     //animateIn => 'fadeIn',
     loop => true,
     autoplay => true,
     dots => false
   ], true);
 });
};
