({
    mustDeps: [
      {block: 'container'}
    ],
    shouldDeps: [
      {elem: 'container'},
      {block: 'slide', mods: {'hasdesc': true}}
    ]
})
