/* global modules:false */

modules.define('slide_hasdesc', ['functions__mobile-detect', 'i-bem__dom'], function(provide, Md, BEMDOM) {

provide(BEMDOM.decl({block: 'slide', modName: 'hasdesc', modVal: true},{
  'onSetMod': {
    'js': {
      'inited': function(){
        this.hidesc = this.findBlockOn('hidesc');
        if(Md.mobile()){
          this.showInfo();
        }
      }
    }
  },
  hideInfo: function(){
    this.hidesc.hide();
  },
  showInfo: function(){
    this.hidesc.show();
  },
},{
  live: function(){
    if(Md.mobile()){
      return false;
    }else{
      this
        .liveBindTo('mouseout', function(){this.hideInfo()})
        .liveBindTo('mouseover', function(){this.showInfo()});
    }
  }
}));

});

