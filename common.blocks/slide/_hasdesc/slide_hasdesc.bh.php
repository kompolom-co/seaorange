<?php
return function ($bh) {
 $bh->match("slide_hasdesc", function ($ctx, $json){
   $ctx->js(true);
   $ctx->mix(['block'=>'hidesc', 'js'=>true]);
 });

 $bh->match("slide_hasdesc__description", function ($ctx, $json){
   $ctx->mix(['block'=>'hidesc', 'elem'=>'desc']);
 });
};
