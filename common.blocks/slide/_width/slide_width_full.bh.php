<?php
return function ($bh) {
 $bh->match("slide_width_full", function ($ctx, $json){
   $ctx
     ->attr('style', 'background-image:url('.$ctx->param('url').')')
     ->mix(['block'=>'slide','elem'=>'image']);
   $ctx->content([
     //[
       //'block' => 'image',
       //'mix'=>['block'=> 'slide','elem'=>'image'],
       //'url' => $ctx->param('url')
     //],
     [
       'elem'=>'container',
       'content'=>[
         [
           'elem' => 'inner',
           'content' =>[
             $ctx->content()
           ] 
         ]
       ]
     ]
    ] ,true);
 });
};
