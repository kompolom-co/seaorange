<?php
return function ($bh) {
 $bh->match("site-preload", function ($ctx, $json){
   $ctx->js(true);
   $ctx->content([
     'block' => 'preloader',
     'mix' =>['block'=>'site-preload', 'elem'=>'svg']
   ]);
 });
};
