/* global modules:false */

modules.define('site-preload',['i-bem__dom'], function(provide, BEMDOM) {

provide(BEMDOM.decl('site-preload', {
  onSetMod: {
    'js': {
      'inited': function(){
        this.setMod('visible', 'no');
      }
    }
  }
}));

});

