({
    mustDeps: [],
    shouldDeps: [
      {block: 'resort-info'},
      {block: 'functions', elem: 'mobile-detect'},
      {block: 'animate'}
    ]
})
