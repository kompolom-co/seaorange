/* global modules:false */

modules.define('resort', ['functions__mobile-detect', 'i-bem__dom'],
               function(provide, MD, BEMDOM) {

provide(BEMDOM.decl('resort', {
  'onSetMod': {
    'js': {
      'inited': function() {
        var event = MD.mobile()? ['click', null]: ['mouseover', 'mouseout'];

        this.info = this.findBlockInside('resort-info');
        this.info.setMod('visible', 'no');

        //show event
        this.bindTo('frame', event[0], this._showInfo);
        //hide event
        this.bindTo('frame', event[1], this._hideInfo);
      }
    }
  },
  _showInfo: function() {
    this.info.setMod('visible', 'yes');
  },
  _hideInfo: function() {
    this.info.setMod('visible', 'no');
  }
}));

});

