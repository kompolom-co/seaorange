<?php
return function ($bh) {
 $bh->match("resort", function ($ctx, $json){
   $ctx
     ->js(true)
     ->mix([
       ['block' => 'animate', 'js'=>true],
       ['block' => 'scrollspy', 'js'=>true],
     ])
     ->content([
     [
       'elem' => 'frame',
       'content' => [
          [
            'block' => 'image',
            'mix' => ['block' => 'resort', 'elem' => 'image'],
            'url' => $ctx->param('image')
          ],
          [
            'elem'=> 'info-container',
            'content' => $ctx->content()
          ]
       ]
     ],
     [
       'elem' => 'desc',
       'content' => [
          [
            'elem' => 'name',
            'content' => $ctx->param('name')
          ],
          [
            'elem' => 'text',
            'content' => $ctx->param('text')
          ]
       ]
     ]
   ], true);
 });
};
