([
  {
    tech: 'js',
    mustDeps: { tech : 'bemhtml', block : 'link' }
  },
  {
    mustDeps: [
    ],
    shouldDeps: [
      {block: 'functions', elem: 'mobile-detect'},
      {block: 'link'}
    ],
}])
