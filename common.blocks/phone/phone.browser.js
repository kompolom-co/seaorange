/* global modules:false */

modules.define('phone', ['functions__mobile-detect', 'bh', 'i-bem__dom'], function(provide, MobileDetect, BH, BEMDOM) {

provide(BEMDOM.decl('phone',{
  'onSetMod': {
    'js': {
      'inited': function() {
          //TODO  перенести функционал в статические свойства, чтобы выполнять проверку 1 раз для всех экзкмпляров
          this.urlscheme = 'tel';//MobileDetect.mobile() ? 'tel' : 'skype';
          this.setMod('scheme', this.urlscheme);
          BEMDOM.update(this.domElem, BH.apply(
            {
              block: 'link',
              mix: { block: 'phone', elem: 'link'},
              content: this.domElem.html(),
              url: this.urlscheme+":"+this.domElem.html()
            }
          ));
      }
    }
  }
}));

});

