<?php
return function ($bh) {
 $bh->match("counter", function ($ctx, $json){
   $ctx
     ->js(['exp' => $ctx->param('exp')? $json->exp : 1])
     ->content([
       [
         'elem' => 'text',
         'content' => $ctx->param('text')
       ],
       [
         'elem' => 'holder'
       ]
     ]);
 });
};
