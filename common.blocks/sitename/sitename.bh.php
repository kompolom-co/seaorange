<?php
return function ($bh) {
 $bh->match("sitename", function ($ctx, $json){
   $ctx->mix([
     'block' => 'gradt',
     'mods' => ['cs'=>'yellow'],
     'js' => true
   ]);
 });
};
