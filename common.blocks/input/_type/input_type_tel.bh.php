<?php
return function ($bh) {
 $bh->match("input_type_tel", function ($ctx, $json){
   $ctx->js(['mask'=> $ctx->param('mask')? $ctx->param('mask') : '+7 (999) 999 99 99'], true);
 });
 $bh->match('input_type_tel__control', function ($ctx) {
   $ctx->attr('type', 'tel');
 });
};
