<?php
return function ($bh) {
 $bh->match("input_type_textarea__control", function ($ctx, $json){
   $ctx
     ->tag('textarea')
     ->attr('rows', $ctx->param('rows')? $json->rows : 10);
 });
};
