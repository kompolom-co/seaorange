/* global modules:false */

modules.define('modal', function(provide, Modal) {

provide(Modal.decl('modal',{
  close: function(){
    this.delMod('visible');
  }
},{
  live: function(){
    this.liveBindTo('close', 'click', function(){this.close()})
  }
}));

});

