<?php
return function ($bh) {
 $bh->match("beach", function ($ctx, $json){
   $ctx
     ->js(['id'=>$json->id])
     ->content([
     [
        'block' => 'image',
        'url' => $ctx->param('image'),
        'alt' => $ctx->param('name')
     ],
     [
       'elem' => 'desc',
       'content' => [
         'elem'=> 'text',
         'content' => [
           $ctx->param('name'),
           '&thinsp;',
           $ctx->param('price'),
         ]
       ]
     ] 
   ], true);
 });
};
