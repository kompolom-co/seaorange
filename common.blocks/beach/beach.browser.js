/* global modules:false */

modules.define('beach',
               ['functions__mobile-detect','jquery', 'BEMHTML', 'bl-client', 'i-bem__dom'],
               function(provide, Md, $, BEMHTML, loader, BEMDOM) {

provide(BEMDOM.decl('beach',{
  'onSetMod': {
    'js': {
      'inited': function(){
      }
    }
  },
  _toggleInfo: function(){
    !this.hasMod('hovered')?
      this.setMod('hovered', true):
      this.delMod('hovered');
  },
  hideInfo: function(){
    this.delMod('hovered');
  },
  showInfo: function(){
    this.setMod('hovered');
  },

  _renderModal: function(content){
    var bemjson = {
      block: 'modal',
      zIndexGroupLevel: 1,
      mix: {block: 'offer-modal', js: true},
      content: [
        {
          block: 'offer-modal',
          elem: 'content',
          content:[
            {
              block: 'row',
              mods: {of: 2},
              content: [
                {
                  elem: 'col',
                  mods: {mw: 4},
                  content: [
                    {
                      block: 'slider',
                      mix: {block: 'offer-modal', elem: 'slider'},
                      mods: {type: 'owl', size:'l', pos: 'aventura'},
                      content: content.images.map(function(img){
                        return {
                          block: 'slide',
                          content: {
                            block: 'link',
                            attrs: {rel: this._uniqId},
                            mix: {block: 'gallery', js: true},
                            url: img,
                            content: {
                              block: 'image',
                              url: img
                            }
                          }
                        }
                      }, this)
                    },
                    {
                      block: 'offer-modal',
                      elem: 'price',
                      content: [
                        'от '+content.price,
                        {
                          elem: 'price-desc',
                          content: content['price-desc']
                        }
                      ]
                    },
                    content.form,
                    //{
                      //block: 'list',
                      //mix:{
                        //block: 'offer-modal',
                        //elem: 'features',
                      //},
                      //content: content.features? content.features.map(function(text){
                        //return {
                          //elem: 'item',
                          //content: text
                        //}
                      //}, this) : ''
                    //}
                  ]
                },
                {
                  elem: 'col',
                  mods: {mw: 8},
                  content: [
                    {
                      block: 'offer-modal',
                      elem: 'name',
                      content: [
                        content.name,
                          {
                            block: 'stars',
                            js: {count: content.stars}
                          },
                      ]
                    },
                    {
                      mix: {block: 'article'},
                      block: 'offer-modal',
                      elem: 'article',
                      content: content.text
                    }
                  ]
                },
              ]
            }
          ]
        }
      ]
    };
    this.popup =  this.findBlockOn(
      BEMDOM.append(this.domElem, BEMHTML.apply(bemjson)), 'modal'
    );
    return this.popup;
  },

  showModal: function(content){
    this.getContent().then(
      function(content){
        this.popup || this._renderModal(content);
        this.popup.setMod('visible', true); 
      },
      null,
      this
    );
  },

  /**
   * load modal content From server
   * @return object promise
   */
  getContent: function(){
    return loader.getJson('beaches', this.params.id);
    /*var deffered = vow.defer();
    if (this._modalContent) {
      deffered.resolve(this._modalContent, this);
    } else {
     $.ajax({
        url: '/ajax.php',
        dataType: 'json',
        data: {
          b: 'beaches',
          id: this.params.id,
          json: true
        },
        success: function(res){
          //TODO: test is object
          this._modalContent = res;
          deffered.resolve(res, this);
        },
        context: this
     });
    }

    return deffered.promise();*/
  }
},{
  live: function(){
      if(Md.mobile()){
        this
          .liveBindTo('click', function(){this.showModal()})
      }else{
        this
          .liveBindTo('click', function(){this.showModal()})
          .liveBindTo('mouseout', function(){this.hideInfo()})
          .liveBindTo('mouseover', function(){this.showInfo()});
      }
  }  
}));

});

