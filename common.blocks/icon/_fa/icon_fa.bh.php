<?php
return function ($bh) {
 $bh->match("icon", function ($ctx, $json){

   if ($ctx->mod('fa')) {
       $ctx->mix(['block'=>'fa']);
       $ctx->cls($ctx->cls().' fa-'.$ctx->mod('fa'), true);
   }

 });
};
