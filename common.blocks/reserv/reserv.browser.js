/* global modules:false */

modules.define('reserv',
               ['BEMHTML', 'jquery', 'vow', 'i-bem__dom'],
               function(provide, BEMHTML, $, vow, BEMDOM) {

provide(BEMDOM.decl('reserv', {
  'onSetMod': {
    'js': {
      'inited': function(){}
    }
  },

  _renderModal: function(content){
    var bemjson = {
      block: 'modal',
      zIndexGroupLevel: 1,
      mix: {block: 'offer-modal', js: true},
      content: [
        {
          block: 'offer-modal',
          elem: 'content',
          content:[
            {
              block: 'row',
              mods: {of: 2},
              content: [
                {
                  elem: 'col',
                  mods: {mw: 3},
                  content: [
                    {
                      block: 'slider',
                      mods: {type: 'owl', size:'m', pos: 'aventura'},
                      content: content.images.map(function(img){
                        return {
                          block: 'slide',
                          content: {
                            block: 'link',
                            attrs: {rel: this._uniqId},
                            mix: {block: 'gallery', js: true},
                            url: img,
                            content: {
                              block: 'image',
                              url: img
                            }
                          }
                        }
                      }, this)
                    },
                    {
                      block: 'offer-modal',
                      elem: 'features',
                      content: content.features? content.features.map(function(text){
                        return [
                          {
                            tag: 'p',
                            content: text.name
                          },
                          {
                            block: 'list',
                            content: text.list.map(function(item){
                              return {
                                elem: 'item',
                                content: item
                              };
                            })
                          }
                        ];
                      }, this) : ''
                    }
                  ]
                },
                {
                  elem: 'col',
                  mods: {mw: 5},
                  content: [
                    {
                      block: 'offer-modal',
                      elem: 'name',
                      content: [
                        content.hotel,
                          {
                            block: 'stars',
                            js: {count: content.stars}
                          },
                      ]
                    },
                    {
                      mix: {block: 'article'},
                      block: 'offer-modal',
                      elem: 'article',
                      content: 
                        content.text instanceof Array?
                          content.text.map( function(p){
                            return {tag: 'p', content: p};
                          }, this) :
                          content.text
                    }
                  ]
                },
                {
                  elem: 'col',
                  mods: {mw: 4},
                  content:[
                    {
                      block: 'offer-modal',
                      elem: 'price',
                      content: [
                        'от '+content.price+"&nbsp;руб",
                        {
                          elem: 'price-desc',
                          content: content['price-desc']
                        }
                      ]
                    },
                    content.form
                  ]
                }
              ]
            }
          ]
        }
      ]
    };
    this.popup =  this.findBlockOn(
      BEMDOM.append(this.domElem, BEMHTML.apply(bemjson)), 'modal'
    );
    return this.popup;
  },

  showModal: function(content){
    this.getContent().then(
      function(content){
        this.popup || this._renderModal(content);
        this.popup.setMod('visible', true); 
      },
      null,
      this
    );
  },

  /**
   * load modal content From server
   * @return object promise
   */
  getContent: function(){
    var deffered = vow.defer();
    if (this._modalContent) {
      deffered.resolve(this._modalContent, this);
    } else {
     $.ajax({
        url: '/ajax.php',
        dataType: 'json',
        data: {
          b: 'reserv',
          json: true
        },
        success: function(res){
          //TODO: test is object
          this._modalContent = res;
          deffered.resolve(res, this);
        },
        context: this
     });
    }

    return deffered.promise();
  }
},{
  live: function(){
    this.liveBindTo('click', function(){this.showModal()});
  }
}));

});

