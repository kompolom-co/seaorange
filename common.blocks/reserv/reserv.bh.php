<?php
return function ($bh) {
 $bh->match("reserv", function ($ctx, $json){
   $ctx
     ->mix([
       ['block' => 'animate', 'js' => ['hide'=>false]],
       ['block' => 'scrollspy', 'js' => true],
     ])
     ->js(true);
 });
};
