({
    mustDeps: [
      {
        block: 'jquery',
        elem: 'event',
        mods: {
          type: 'pointer'
        }
      }
    ],
    shouldDeps: [
      {block: 'bl-client'}
    ]
})
