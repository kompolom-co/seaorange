<?php
return function ($bh) {
 $bh->match("filter-item", function ($ctx, $json){
   $ctx
     ->js(true)
     ->mix(['block' => 'hidesc', 'js' => true])
     ->attr('data-groups',
       json_encode($ctx->param('groups')? $json->groups: array()))
     ->content([
       [
         'elem' => 'wrap',
         'content' => $ctx->content()
       ]
     ], true);
 });
};
