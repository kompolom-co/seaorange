<?php
return function ($bh) {
 $bh->match("filter-item__info", function ($ctx, $json){
   $ctx->mix(['block' => 'hidesc', 'elem' => 'desc']);
 });
};
