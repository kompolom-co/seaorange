/* global modules:false */

modules.define('filter-item',
               ['bl-client', 'BEMHTML', 'i-bem__dom'],
               function(provide, loader, BEMHTML, BEMDOM) {

provide(BEMDOM.decl('filter-item', {
  'onSetMod': {
    'js': {
      'inited': function(){
        this.desc = this.findBlockOn('hidesc');
      }
    },
    'desc' : {
      'visible' : function(){
        this.desc.show();
      },
      '' : function(){
        this.desc.hide();
      }
    }
  },

  showModal: function(){
    this.__self.getContent(this.params.id).then(
      function(content){
        this.popup || this._renderModal(content);
        debugger
        this.popup.setMod('visible', true); 
        var slider = this.popup.findBlockInside('slider');
        this.popup.domElem.trigger('resize');
        slider.reload();
      },
      null,
      this
    ); 
  },

  _renderModal: function(content){
    var bemjson = {
      block: 'modal',
      zIndexGroupLevel: 1,
      mix: {block: 'offer-modal', js: true},
      content: [
        {
          block: 'offer-modal',
          elem: 'content',
          content:[
            {
              block: 'row',
              mods: {of: 2},
              content: [
                {
                  elem: 'col',
                  mods: {mw: 4},
                  content: [
                    content.tags ? {
                      block: 'offer-modal',
                      elem: 'tags',
                      content: content.tags.map(function(tag){
                        return {
                          elem: 'tag',
                          content: tag
                        }
                      }, this)
                      } : null,
                    {
                      tag: 'div',
                      content: [
                        {
                          block: 'slider',
                          mods: {type: 'bx'},
                          js: {
                            pager: false,
                            adaptiveHeight: true,
                            controls: false,
                            slideSelector: '.slide',
                            infiniteLoop: true,
                            auto: true,
                            autoHover: true
                          },
                          content: content.images.map(function(img){
                            return {
                              block: 'slide',
                              content: {
                                block: 'link',
                                attrs: {rel: this._uniqId},
                                mix: {block: 'gallery', js: true},
                                url: img,
                                content: {
                                  block: 'image',
                                  url: img
                                }
                              }
                            }
                          }, this)
                        },
                      ]
                    },
                    {
                      block: 'offer-modal',
                      elem: 'features',
                      content: content.features? content.features.map(function(text){
                        return [
                          {
                            tag: 'p',
                            content: text.name
                          },
                          {
                            block: 'list',
                            content: text.list.map(function(item){
                              return {
                                elem: 'item',
                                content: item
                              };
                            })
                          }
                        ];
                      }, this) : ''
                    }
                  ]
                },
                {
                  elem: 'col',
                  mods: {mw: 4},
                  content: [
                    {
                      block: 'offer-modal',
                      elem: 'name',
                      content: [
                        content.name,
                          {
                            block: 'stars',
                            js: {count: content.stars}
                          },
                      ]
                    },
                    {
                      mix: {block: 'article'},
                      block: 'offer-modal',
                      elem: 'article',
                      content: content.text
                    }
                  ]
                },
                {
                  elem: 'col',
                  mods: {mw: 4},
                  content:[
                    {
                      block: 'offer-modal',
                      elem: 'price',
                      content: [
                        'от ',
                        content.price,
                        ' руб.',
                        {
                          elem: 'price-desc',
                          content: content['price-desc']
                        }
                      ]
                    },
                    content.form
                  ]
                }
              ]
            }
          ]
        }
      ]
    };
    this.popup =  this.findBlockOn(
      BEMDOM.append(this.domElem, BEMHTML.apply(bemjson)), 'modal'
    );
    return this.popup;
  },

},{

  getContent: function(id){
    return loader.getJson('hotels', id);
  },

  live: function(){
    this.liveBindTo('mouseover mouseout', function(){
      this.toggleMod('desc', 'visible', '');
    });

    this.liveBindTo('click', function(){
      this.showModal();
    });
  }
}));

});

