/* global modules:false */

modules.define('hidesc',['i-bem__dom'], function(provide, BEMDOM) {

provide(BEMDOM.decl('hidesc',{
  onSetMod: {
    'js': {
      'inited': function(){
        this._prop = this.params.prop || 'bottom';
        this.setPos();
      }
    }
  },
  setPos: function(){
    var height = this.elem('desc').innerHeight();
    this.elem('desc').css(this._prop , -height+'px');
  },
  show: function(){
    this.setMod(this.elem('desc'), 'visible', true);
  },
  hide: function(){
    this.delMod(this.elem('desc'), 'visible');
  },
},{
  live: function(){
    this.liveBindTo('resize', function(){
      this.setPos();
    });
    return false;
  }
}));

});

