/* global modules:false */

modules.define('resort-info',
               ['BEMHTML', 'jquery', 'bl-client', 'i-bem__dom'],
               function(provide, BEMHTML, $, api, BEMDOM) {

provide(BEMDOM.decl('resort-info',{
  'onSetMod': {
    'js': {
      'inited': function(){}
    }
  },

  _renderModal: function(content){
    var bemjson = {
      block: 'modal',
      zIndexGroupLevel: 1,
      mix: {block: 'offer-modal', js: true},
      content: [
        {
          block: 'offer-modal',
          elem: 'content',
          content:[
            {
              block: 'row',
              mods: {of: 2},
              content: [
                {
                  elem: 'col',
                  mods: {mw: 4},
                  content: [
                    {
                      block: 'slider',
                      mods: {type: 'owl', size:'l', pos: 'aventura'},
                      content: content.images.map(function(img){
                        return {
                          block: 'slide',
                          content: {
                            block: 'link',
                            attrs: {rel: this._uniqId},
                            mix: {block: 'gallery', js: true},
                            url: img,
                            content: {
                              block: 'image',
                              url: img
                            }
                          }
                        }
                      }, this)
                    },
                    {
                      block: 'offer-modal',
                      elem: 'features',
                      content: content.features? content.features.map(function(text){
                        return [
                          {
                            tag: 'p',
                            content: text.name
                          },
                          {
                            block: 'list',
                            content: text.list.map(function(item){
                              return {
                                elem: 'item',
                                content: item
                              };
                            })
                          }
                        ];
                      }, this) : ''
                    }
                  ]
                },
                {
                  elem: 'col',
                  mods: {mw: 4},
                  content: [
                    {
                      block: 'offer-modal',
                      elem: 'name',
                      content: [
                        content.name,
                          {
                            block: 'stars',
                            js: {count: content.stars}
                          },
                      ]
                    },
                    {
                      mix: {block: 'article'},
                      block: 'offer-modal',
                      elem: 'article',
                      content: [
                        content.text,

                      ]
                    }
                  ]
                },
                {
                  elem: 'col',
                  mods: {mw: 4},
                  content:[
                    {
                      block: 'offer-modal',
                      elem: 'price',
                      content: [
                        'от ',
                        content.price,
                        " руб.",
                        {
                          elem: 'price-desc',
                          content: [
                            this.elem('time').html(),
                            ' ½DBL'
                          ]
                        }
                      ]
                    },
                    content.form
                  ]
                }
              ]
            }
          ]
        }
      ]
    };
    this.popup =  this.findBlockOn(
      BEMDOM.append(this.domElem, BEMHTML.apply(bemjson)), 'modal'
    );
    return this.popup;
  },

  showModal: function(content){
    this.getContent().then(
      function(content){
        this.popup || this._renderModal(content);
        this.popup.setMod('visible', true); 
      },
      null,
      this
    );
  },

  /**
   * load modal content From server
   * @return object promise
   */
  getContent: function(){
    return api.getJson('hotels', this.params.hotel);
  }

},{
  live: function(){
      this.liveBindTo('order','click', function(){this.showModal()})
  }
}));

});

