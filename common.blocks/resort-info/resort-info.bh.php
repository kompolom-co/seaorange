<?php
return function ($bh) {
 $bh->match("resort-info", function ($ctx, $json){
   $ctx
     ->js(['id' => $json->id])
     ->content([
     $ctx->content(),
    [
      'block'=>'button',
      'mix' => [
        ['block' => 'resort-info', 'elem' => 'order'],
      ],
      'mods' => ['theme' => 'black', 'size'=> 'x'],
      'text' => 'узнать больше'
    ]
   ], true);
 });
};
