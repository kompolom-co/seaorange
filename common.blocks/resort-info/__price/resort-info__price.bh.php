<?php
return function ($bh) {
 $bh->match("resort-info__price", function ($ctx, $json){
   $ctx->content([
     "от ",
     [
       'elem'=> 'p-count',
       'tag' => 'span',
       'content'=> $ctx->param('count')."&nbsp;"
     ],
     $ctx->param('curr')
   ]);
 });
};
