/* global modules:false */

modules.define('functions__mobile-detect', function(provide) {

/* borschik:include:../../libs/mobile-detect/mobile-detect.js */
var md = new MobileDetect(window.navigator.userAgent);
provide(md);

});

