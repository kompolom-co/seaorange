<?php
return function ($bh) {
 $bh->match("animate", function ($ctx, $json){
   $ctx->js(true);
   $ctx->mix(['block' => 'scrollspy']);
 });
};
