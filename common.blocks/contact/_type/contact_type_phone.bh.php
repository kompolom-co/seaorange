<?php
return function ($bh) {
 $bh->match("contact_type_phone", function ($ctx, $json){
   $ctx->content([
     'block' => 'phone',
     'content' => $ctx->content()
   ], true);
 });
};
