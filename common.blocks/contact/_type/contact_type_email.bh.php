<?php
return function ($bh) {
 $bh->match("contact_type_email", function ($ctx, $json){
   $ctx
     ->tag('address')
     ->content([
       [
         'block' => 'contacts',
         'elem' => 'label',
         'content' => 'E-mail:&nbsp;',
         'tag' => 'span'
       ],
       [
         'block' => 'link',
         'mix' => ['block'=>'contact', 'elem'=>'link'],
         'url' => 'mailto:'.$ctx->content(),
         'content' => $ctx->content()
       ]
     ], true);
 });
};
