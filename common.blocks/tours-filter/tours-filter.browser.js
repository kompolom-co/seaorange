/* global modules:false */

modules.define('tours-filter',
               ['shuffle', 'i-bem__dom'],
               function(provide, $, BEMDOM) {

provide(BEMDOM.decl('tours-filter',{
  'onSetMod': {
    'js': {
      'inited': function(){
        this.elem('items').shuffle({
          itemSelector: '.filter-item'
        });
        this.bindTo('criteria', 'click', this._filterItems);
        this.links = this.findBlocksInside('filters', 'link'); 
      }
    }
  },
  _filterItems: function(e) {
    var link = this.findBlockOn($(e.currentTarget), 'link');
    this._enableLinks();
    link.setMod('disabled', true);
    this.elem('items').shuffle('shuffle', link.getUrl());
  },

  _enableLinks: function(){
    this.links.map(function(item){
      item.delMod('disabled');
    }, this);
  }
}));

});

