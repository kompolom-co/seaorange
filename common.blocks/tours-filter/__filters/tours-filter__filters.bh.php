<?php
return function ($bh) {
 $bh->match("tours-filter__filters", function ($ctx, $json){
   $ctx
     ->tag('span')
     ->content(
       array_map(function($item){
          return [
            'elem' => 'criteria',
            'name' => $item['name'],
            'val' => $item['val']
          ];
       }, $ctx->param('filters')));
 });
};
