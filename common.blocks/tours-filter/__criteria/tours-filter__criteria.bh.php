<?php
return function ($bh) {
 $bh->match("tours-filter__criteria", function ($ctx, $json){
   //$ctx
     //->tag('li')
     //->attr('data-group', $ctx->param('val'))
     //->content($ctx->param('name'));
   return [
     'block' => 'link',
     'mix' => [
       'block' => 'tours-filter',
       'elem' => 'criteria'
     ],
     'mods' => ['pseudo' => true],
     'content' => $ctx->param('name'),
     'url' => $ctx->param('val')
   ];
 });
};
