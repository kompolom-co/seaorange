/* global modules:false */

modules.define('screen-h', ['jquery', 'i-bem__dom'], function(provide, $, BEMDOM) {

provide(BEMDOM.decl({block: 'screen-h'},{
  onSetMod: {
    'js' : {
      'inited': function(){
        this.bindTo($(window), 'resize', this._setHeight, this);
        this._setHeight();
      }
    }
  },
  _offset: 0,
  /**
   * Set block height as window heigh
   * @returns {void}
   */
  _setHeight: function(){
    this.height = $(window).height();
    this.domElem.css(this._prop(), this.height - this._offset);
    this.emit('change');
  },

  /**
   * Returns block height
   * @returns {int} block height
   */
  getHeight: function(){
    return this.height;
  },

  /**
   * set block offset
   * @param {int} newOffset block offset
   */
  setOffset: function(newOffset){
    this._offset = newOffset;
    this._setHeight();
  },
  _prop: function(){return'height'}
},{
}));

});

