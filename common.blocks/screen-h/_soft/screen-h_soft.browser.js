/* global modules:false */

modules.define('screen-h', function(provide, BEMDOM) {

provide(BEMDOM.decl({modName: 'soft', modVal: true},{
  onSetMod: {
    'js': {
      'inited': function(){
        this.__base.apply(this, arguments);
      }
    }
  },

  _prop: function(){return 'min-height';}
},{
}));

});

