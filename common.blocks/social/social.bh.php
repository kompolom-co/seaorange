<?php
return function ($bh) {
 $bh->match("social", function ($ctx, $json){
   $ctx->content(
     [
        [
          'elem'=>'list',
          'tag'=>'ul',
          'content'=>array_map(function($link){
            return [
              'elem'=>'item',
              'tag'=>'li',
              'content'=>[
                [
                  'block'=>'link',
                  'target'=>'_blank',
                  'url'=>$link['url'],
                  'cls' => 'fa-stack fa-lg',
                  'content'=>[
                    [
                      'block' => 'icon',
                      'cls' => 'fa-stack-1x',
                      'mods' => ['fa' => $link['name']]
                    ]
                  ]
                ]
              ]
            ];
          }, $ctx->param('links'))
        ]
     ]
   ); 
 });
};
