<?php
return function ($bh) {
 $bh->match("f-menu__inner", function ($ctx, $json){

   return[
     [
       'block' => 'popup',
       'js'=>true,
       'mods' => ['target'=>'position', 'theme' => 'f-menu'],
       'zIndexGroupLevel' => 9,
       'mix' => ['block' => 'f-menu', 'elem' => 'inner'],
       'content' => [
           [
             'block' => 'button',
             'text' => '<',
             'mods' => ['theme' => 'white'],
             'mix' => ['block' => 'popup', 'elem' => 'close']
           ],
           $ctx->content(),
           /*[
             'block' => 'f-menu',
             'elem' => 'shape',
             'attrs' => [
               'data-open' =>   'M300-10c0,0,295,164,295,410c0,232-295,410-295,410',
               'data-closed' => 'M300-10C300-10,5,154,5,400c0,232,295,410,295,410',
             ],
             'content' => [
               [
                 'block' => 'f-menu',
                 'elem' => 'svg',
                 'tag' => 'svg',
                 'attrs' => [
                    'width' => '100%',
                    'height' => '100%',
                    'viewBox' => '0 0 600 800',
                    'preserveAspectRatio' => 'none'
                  ],
                  'content' => [
                    'block' => 'f-menu',
                    'elem' => 'svgpath',
                    'tag' => 'path',
                    'attrs' => [
                      'fill' => 'none',
                      'd' => 'M300-10c0,0,0,164,0,410c0,232,0,410,0,410'
                    ]
                  ]
               ]
             ]
           ]*/
       ]
     ],
   ];
 });
};
