<?php
return function ($bh) {
 $bh->match("f-menu__handle", function ($ctx, $json){
     return [
       'block' => 'button',
       'mix' => ['block' => 'f-menu', 'elem' => 'handle'],
       'text' => $ctx->content()
     ]; 
 });
};
