/* global modules:false */

modules.define('f-menu',
               ['i-bem__dom'],
               function(provide, BEMDOM) {

provide(BEMDOM.decl('f-menu', {
  'onSetMod': {
    'js': {
      'inited': function(){
        this.popup = this.findBlockOn('inner','popup');
        this.popup.setPosition(0, 0);
        this.popup.delMod('visible');
        this.popup.on('close', this.toggle, this);

        this.trigger = this.elem('handle');
        this.shapeEl = this.elem('shape');

        /*var s = Snapsvg.Snap(this.elem('svg')[0]);
        console.log(Snapsvg);*/

        /*this.pathEl = s.select('path');
        this.paths = {
          reset : this.pathEl.attr( 'd' ),
          open : this.shapeEl.attr( 'data-open' ),
          close : this.shapeEl.attr( 'data-closed' )
        };*/
        this.isOpen = false;
        this.initEvents();
      }
    },
    'opened': {
      'true' : function(){
        this.popup.setMod('visible', true);
        this.setMod(this.trigger, 'opened', true);
      },
      ''     : function(){
        this.popup.delMod('visible');
        this.setMod(this.trigger, 'opened', false);
      }
    }
  },
  initEvents: function(){
     this.findBlockOn(this.trigger, 'button').on('click', this.toggle, this);
  },
  toggle: function(){
    var self = this;
    if( this.isOpen ) {
      this.delMod('opened');
      //setTimeout( function() { classie.remove( self.el, 'menu--open' ); }, 250 );
    }
    else {
      this.setMod('opened', true);
      //setTimeout( function() { classie.add( self.el, 'menu--open' );  }, 250 );
    }
    //this.pathEl.stop().animate( { 'path' : this.isOpen ? this.paths.close : this.paths.open }, 350, Snapsvg.mina.easeout, function() {
      //self.pathEl.stop().animate( { 'path' : self.paths.reset }, 800, Snapsvg.mina.elastic );
    //} );
    
    this.isOpen = !this.isOpen;
  }
},{
  live: function(){
    this.liveInitOnEvent('handle', 'pointerover');
  }
}));

});

