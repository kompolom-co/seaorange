<?php
return function ($bh) {
 $bh->match("count", function ($ctx, $json){
   $ctx
      ->js(['count' => $json->count])
      ->mix([
        ['block' => 'animate', 'js'=> true],
        ['block' => 'scrollspy', 'js'=> true],
      ])
      ->content([
        [
          'elem' => 'count',
          'attrs' => ['data-count' => $json->count]
        ],
        [
          'elem' => 'text',
          'content' => $ctx->param('text')
        ]
      ]);
 });
};
