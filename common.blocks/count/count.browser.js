/* global modules:false */

modules.define('count', ['jquery','i-bem__dom'], function(provide, $, BEMDOM) {

provide(BEMDOM.decl('count', {
  'onSetMod': {
    'js': {
      'inited': function(){
          this._curr = 0;
          this.count = this.params.count;
          this.duration = this.params.duration || 2000;
          this.findBlockOn('scrollspy').on('scrollin', this.start, this);
          this.findBlockOn('scrollspy').on('scrollout', this.reset, this);
      }
    }
  },
  start: function(e, dir){
    if (dir === "up" ) { return; } 
    this.elem('count').css({num: this._curr}).animate({num: this.count}, {
      duration: this.duration,
      step: function(now){
        $(this).text(Math.floor(now));
      },
    });
  },
  reset: function(e, dir){
    if (dir === "down" ) { return; } 
    this._curr = 0;
    this.elem('count').text(this._curr);
  }
}));

});

