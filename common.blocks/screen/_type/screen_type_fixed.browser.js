/* global modules:false */

modules.define('sceen_type_fixed',['i-bem__dom'], function(provide, BEMDOM) {

provide(BEMDOM.decl({block:'screen', modName: 'type', modVal:'fixed'},{
  onSetMod: {
    'js': {
      'inited': function(){
      }
    }
  },
}));

});

