<?php
return function ($bh) {
 $bh->match("screen_type_fixed", function ($ctx, $json){
   $ctx
     ->content([
       [
         'elem' => 'wrap',
         'content' =>$ctx->content()
       ], 
     ], true)
     ->js(true)
     ->mix([
        'block' => 'screen-h',
        'js' => true
     ]);
 });
};
