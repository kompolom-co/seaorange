/* global modules:false */

modules.define('popup', function(provide, Popup) {

provide(Popup.decl({block: 'popup', modName: 'theme', modVal:'f-menu'},{
  'onSetMod': {
    'js': {
      'inited': function(){
        this.__base.apply(this, arguments);
        this.bindTo(this.elem('close'), 'click', function(){
          this.emit('close');
          this.delMod('visible');
        });
      }
    }
  }
}));

});

