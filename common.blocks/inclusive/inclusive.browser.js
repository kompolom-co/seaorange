/* global modules:false */

modules.define('inclusive',
               ['functions__mobile-detect', 'BEMHTML', 'jquery', 'bl-client', 'i-bem__dom'],
               function(provide, Md, BEMHTML, $, API, BEMDOM) {

provide(BEMDOM.decl('inclusive', {
  'onSetMod': {
    'js': {
      'inited': function(){
        this.button = this.findBlockInside('button');
        this.button.on('click', this.showModal, this);
      }
    }
  },
  _toggleInfo: function(){
    !this.hasMod('hovered')?
      this.setMod('hovered', true):
      this.delMod('hovered');
  },
  hideInfo: function(){
    this.delMod('hovered');
  },
  showInfo: function(){
    this.setMod('hovered');
  },

  _renderModal: function(content){
    var bemjson = {
      block: 'modal',
      zIndexGroupLevel: 1,
      mix: {block: 'offer-modal', js: true},
      content: [
        {
          block: 'offer-modal',
          elem: 'content',
          content:[
            {
              block: 'row',
              mods: {of: 2},
              content: [
                {
                  elem: 'col',
                  mods: {mw: 4},
                  content: [
                    content.tags ? {
                      block: 'offer-modal',
                      elem: 'tags',
                      content: content.tags.map(function(tag){
                        return {
                          elem: 'tag',
                          content: tag
                        }
                      }, this)
                      } : null,
                    {
                      block: 'slider',
                      mods: {type: 'owl', size:'l', pos: 'aventura'},
                      content: content.images.map(function(img){
                        return {
                          block: 'slide',
                          content: {
                            block: 'link',
                            attrs: {rel: this._uniqId},
                            mix: {block: 'gallery', js: true},
                            url: img,
                            content: {
                              block: 'image',
                              url: img
                            }
                          }
                        }
                      }, this)
                    },
                    {
                      block: 'offer-modal',
                      elem: 'features',
                      content: content.features? content.features.map(function(text){
                        return [
                          {
                            tag: 'p',
                            content: text.name
                          },
                          {
                            block: 'list',
                            content: text.list.map(function(item){
                              return {
                                elem: 'item',
                                content: item
                              };
                            })
                          }
                        ];
                      }, this) : ''
                    }
                  ]
                },
                {
                  elem: 'col',
                  mods: {mw: 4},
                  content: [
                    {
                      block: 'offer-modal',
                      elem: 'name',
                      content: [
                        content.name,
                          {
                            block: 'stars',
                            js: {count: content.stars}
                          },
                      ]
                    },
                    {
                      mix: {block: 'article'},
                      block: 'offer-modal',
                      elem: 'article',
                      content: content.text
                    }
                  ]
                },
                {
                  elem: 'col',
                  mods: {mw: 4},
                  content:[
                    {
                      block: 'offer-modal',
                      elem: 'price',
                      content: [
                        'от ',
                        content.price,
                        ' руб.',
                        {
                          elem: 'price-desc',
                          content: content['price-desc']
                        }
                      ]
                    },
                    content.form
                  ]
                }
              ]
            }
          ]
        }
      ]
    };
    this.popup =  this.findBlockOn(
      BEMDOM.append(this.domElem, BEMHTML.apply(bemjson)), 'modal'
    );
    return this.popup;
  },

  showModal: function(content){
    this.getContent().then(
      function(content){
        this.popup || this._renderModal(content);
        this.popup.setMod('visible', true); 
      },
      null,
      this
    );
  },

  /**
   * load modal content From server
   * @return object promise
   */
  getContent: function(){
    return API.getJson('inclusive-desc', this.params.id);
  }

},{
  live: function(){
      if(Md.mobile()){
        this
          .liveBindTo('click', function(){this._toggleInfo()})
      }else{
        this
          .liveBindTo('mouseout', function(){this.hideInfo()})
          .liveBindTo('mouseover', function(){this.showInfo()});
      }
  }  

}));

});

