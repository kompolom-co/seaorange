<?php
return function ($bh) {
 $bh->match("inclusive", function ($ctx, $json){
   $ctx
     ->js(['id'=> $ctx->param('id')])
     ->mix(['block'=>'animate', 'js'=>true])
     ->content([
     [
       'elem' => 'wrap',
       'content' => [
         [
           'block' => 'image',
           'url' => $ctx->param('image'),
           'alt' => $ctx->param('name'),
           'mix' => ['block'=> 'inclusive', 'elem'=>'image']
         ],
         [
           'elem' => 'price',
           'content' => $ctx->param('price')
         ]
       ]
     ], //wrap
     [
        'elem' => 'name',
        'content' => $ctx->param('name')
     ], //name
     [
       'block' => 'button',
       'text' => $ctx->param('button'),
       'mods' => [
         'size' => 'l',
         'theme' => 'white',
       ]
     ]
   ]);
 });
};
