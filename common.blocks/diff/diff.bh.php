<?php
return function ($bh) {
 $bh->match("diff", function ($ctx, $json){
   $ctx->content([
     [
       'elem' => 'icon',
       'content' =>
         [
           'block' => 'icon',
           'mods' => ['ico' => $ctx->param('icon')],
         ],
     ],
     [
       'elem' => 'text',
       'content' => $ctx->param('content')
     ]
   ], true)
   ->mix([
     'block' => 'row',
     'elem' => 'col',
     'mods' => ['mw' => 6, 'sw' => 12]
   ]);
 });
};
