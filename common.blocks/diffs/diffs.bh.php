<?php
return function ($bh) {
 $bh->match("diffs", function ($ctx, $json){
   $ctx->mix(['block' => 'row', 'mods' => ['sac' => true]]);
 });
};
