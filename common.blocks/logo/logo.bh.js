module.exports =
 function (bh) {
 bh.match("logo", function (ctx, json){
   ctx.content([
     {
       "block" : "image",
       "mix" : {'block': 'logo', "elem" : "image"},
       "url" : ctx.param('url'),
       'alt' : ctx.param('name')
     },
     {
       'elem' : 'text',
       'content' : [
          {
            'elem' : 'name',
            'content' : ctx.param('name')
          },
          {
            'elem' : 'desc',
            'content' : ctx.param('desc')
          },
       ]
     }
   ]);
 });
};
