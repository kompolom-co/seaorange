<?php
return function ($bh) {
 $bh->match("map", function ($ctx, $json){
   $ctx->js([
     'center' => $ctx->param('center'),
     'zoom' => $ctx->param('zoom'),
     'mark' => $ctx->param('mark'),
   ], true);
 });
};
