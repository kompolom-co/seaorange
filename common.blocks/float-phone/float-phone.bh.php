<?php
return function ($bh) {
 $bh->match("float-phone", function ($ctx, $json){
   $ctx
     ->js(true)
     ->content([
     [
       'block' => 'fa',
       'cls' => 'fa-phone',
       'mix'=>['block'=>'float-phone','elem'=> 'phone'],
     ],
     [ 'elem' => 'ripple' ], 
     [ 'elem' => 'ripple' ], 
     [ 'elem' => 'ripple' ], 
   ]);
 });
};
