/* global modules:false */

modules.define('float-phone',['functions__mobile-detect','jquery', 'i-bem__dom'], function(provide, Md, $, BEMDOM) {

provide(BEMDOM.decl('float-phone', {
    onSetMod: {
      'js': {
        'inited': function(){
          if (!Md.mobile()) {
            this.setMod('hidden', true);
          }else{
            this.bindTo($(document),'pointerdown', this._onPointer);
            this.bindTo($(document),'pointerup', this._onPointerend);
          }
        }
      }
    },
    _onPointer: function(e){
      if (!$(e.target).closest(this.domElem).length){
        this.setMod('hidden', true);
      }
    },
    _onPointerend: function(e){
      this.setMod('hidden', false);
    }
}));

});

