<?php

/**
 *  Инициализация приложения
 *  @package Easyland/core
 *  @version 0.3.1
 */
define('DS', DIRECTORY_SEPARATOR);


require 'settings.php';
require 'lib/Arr.php';
require 'lib/Form.class.php';
require 'lib/Field.php';

$content = (include_once "content.php");
$bundles = array('index','thanks');
$bundle = 'index';
//set route
$route = explode('/', trim($_SERVER['REQUEST_URI']));

if (in_array($route[1], $bundles)) {
    $bundle = $route[1];
}

if (array_key_exists('page', $_REQUEST)) {
    $bundle = $_REQUEST['page'];
}

/*Language loader*/
if ($lang) {
    $l = array();
    $l['content'] = "content.$lang.json";
    $l['formsrc'] = "forms.$lang.json";
    $l['fieldsrc'] = "fields.$lang.json";

    foreach ($l as $source => $file) {
        if (file_exists($file)) {
            $langContent = json_decode(file_get_contents($file), true);
            $$source = array_replace_recursive($$source, $langContent);
        }
    }
}
function utm_marks(array $utm){
        foreach ($utm as $mark =>$name){
          if(array_key_exists($mark, $_GET))
          {
                echo "<input type='hidden' name='$mark' value='$_GET[$mark]' />\n";
          }
        }
  }
function statistic($utm){
  utm_marks($utm);
  $ref = Arr::get($_SERVER, 'HTTP_REFERER');
    echo "<input type=hidden name='ip' value='".$_SERVER['REMOTE_ADDR']."' />\n";
    echo "<input type=hidden name='referer' value='".$ref."' />\n";
}

//Define bem constants
define('b','block');
define('block','block');
define('elem','elem');
define('e','elem');
define('m','mods');
define('mods','mods');
define('mix','mix');
define('c','content');
define('content','content');
define('js','js');
