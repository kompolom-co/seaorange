<?php
require "lib/libmail.php";
require "lib/init.php";
require "lib/form.php";
require "lib/formValidator.php";
require "lib/rules.php";
include "lib/SxGeo.php";
//require "lib/smsweb.php";

$writer = frontData::getInstance($fields,'post');


$m = new Mail("utf-8");

function get_translate($value, $array){
    if(array_key_exists($value, $array)){
        $trans = $array[$value];
        return $trans;
    }else{
        return $value;
    }
}
/**
 * Установка Адресатов письма
 */
function set_address($mailer,$addresses){
  if(is_array($addresses)){
    foreach ($addresses as $address){
      $mailer->To($address);
    }
  }else{
      $mailer->To($addresses); 
  }
}

$formid= $writer->get_field('formid');

$form = Form::getInstance($formid);
$formname= $form->get_name();

$writer->update_field('formid', $formname);



//перевод полей формы
foreach ($form->get_fields() as $fieldObj){
  $writer->add_field($fieldObj->get_name());
  $writer->add_translation($fieldObj->get_name(), $fieldObj->get_fieldname());
}

/*
 * Определяем регион
 */
$ip = $writer->get_field('ip');
$geo = new SxGeo(GEODB);
$city = $geo->getCity($ip);
if ($city){
  $str = $city['city']." (".$city['country'].')';
  $writer->update_field('city', $str);
}

$refecn = $writer->get_field('referer');
$ref = urldecode($refenc);
$writer->update_field('referer', $ref);

 $search = 'none';

  // Это строчка с реферером - URL страницы, с которой
  // посетитель пришёл на сайт
  if(!isset($_SERVER['HTTP_REFERER'])) $_SERVER['HTTP_REFERER'] = "";
  $reff = $writer->get_field('referer');
  // Выясняем принадлежность к поисковым системам
  if(strpos($reff,"yandex"))  $search = 'yandex';
  if(strpos($reff,"rambler")) $search = 'rambler';
  if(strpos($reff,"google"))  $search = 'google';
  if(strpos($reff,"aport"))   $search = 'aport';
  if(strpos($reff,"mail") && strpos($reff,"search"))   $search = 'mail';
  if(strpos($reff,"msn") && strpos($reff,"results"))   $search = 'msn';
  $server_name = $_SERVER["SERVER_NAME"];
  if(substr($_SERVER["SERVER_NAME"],0,4) == "www.")
  {
    $server_name = substr($_SERVER["SERVER_NAME"], 4);
  }
  if(strpos($reff,$server_name)) $search = 'own_site';

  
  //вносим поисковый запрос в соответствующую таблицу
  if(!empty($reff) && $search!="none" && $search != "own_site")
  {
    switch($search)
    {
      case 'yandex':
      {
        preg_match("|text=([^&]+)|is", $reff."&", $out);
        if(strpos($reff,"yandpage")!=null)
          $quer = convert_cyr_string(urldecode($out[1]),"k","w");
        else
          $quer= $out[1];
        break;
      }
      case 'rambler':
      {
        preg_match("|words=([^&]+)|is", $reff."&", $out);
        $quer = $out[1];
        break;
      }
      case 'mail':
      {
        preg_match("|q=([^&]+)|is", $reff."&", $out);
        $quer = $out[1];
        break;
      }
      case 'google':
      {
        preg_match("|[^a]q=([^&]+)|is", $reff."&", $out);
        $quer = utf8_win($out[1]); 
        break;
      }
      case 'msn':
      {
        preg_match("|q=([^&]+)|is", $reff."&", $out);
        $quer = utf8_win($out[1]);
        break;
      }
      case 'aport':
      {
        preg_match("|r=([^&]+)|is", $reff."&", $out);
        $quer = $out[1];
        break;
      }
    }
    $symbols = array("\"", "'", "(", ")", "+", ",", "-"); 
    $quer = str_replace($symbols, " ", $quer); 
    $quer = urldecode($quer);
    $quer = trim($quer); 
    $quer = preg_replace('|[\s]+|',' ',$quer); 
    
    $writer->update_field('referer','<a href="'.$reff.'>"<i>'.$search."</i></a>:".$quer);
  }


$name = $writer->get_field('name');
$phone = $writer->get_field('phone');
$m->From(FROM); // от кого отправляется почта 
set_address($m, $MY_EMAIL); // кому адресованно
$m->Subject( "-$name | $phone ".SITENAME );
$m->Body( "Здравствуйте! <br /> Посетитель вашего сайта хочет сделать заказ.<br />".$writer->write_table()."
","html" );    
$m->Priority(3) ;    // приоритет письма 
if(USE_SMTP){$m->smtp_on(SERVER,LOGIN,PW);}
$m->Send();

$answ = array(
	'msg' => MSG_SUCCESS,
    'status'=>'success'
);

//if (!$m->status_mail['status']){
  //$answ = array(
      //'msg' => $m->status_mail['message'],
      //'status'=>'fail'
  //);  
//}

if ($answ['status'] == 'success') {
    $answ['msg'] = [
        block=> "thankyou",
        logo => $content['logo']['url'],
        'logoname' => $content['logo']['name'],
        'logodesc' => $content['logo']['desc'],
        'sitename' => $content['sitename'],
        'phone'    => $content['phones'][0],
        'managerUrl' => '/img/manager.png',
        'user' => $writer->get_field('name') ,
        'manager' => 'Оксана' 
    ];
}
echo json_encode($answ);
//echo $m->Get();
?> 
