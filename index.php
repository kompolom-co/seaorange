<?php
/**
 * Site entry point
 * @package Haywedding
 * @category Haywedding
 */

require_once "vendor/autoload.php";
require_once 'lib/init.php';

$page = (include "desktop.bundles/$bundle/$bundle.php");

if (php_sapi_name() == 'cli-server') {
    file_put_contents("desktop.bundles/$bundle/$bundle.bemjson.js", '('.json_encode($page, JSON_UNESCAPED_UNICODE).')');
    exec('enb make');
}

//Load templates
require "desktop.bundles/$bundle/$bundle.bh.php";


echo $bh->apply($page);
