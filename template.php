<?php
return array(
    block=> 'page',
    mods=> [preloader=> true],
    mix=> [block=>'scrollspy', js=> true],
    title=> 'SeaOrange',
    styles=> [
        [ elem=> 'meta', attrs=> [name=> 'viewport', content=> 'width=device-width, initial-scale=1,0']],
        [ elem=> 'css', url=> '/desktop.bundles/index/_index.css' ],
        [ elem=> 'css', url=> 'http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|PT+Sans+Narrow&subset=latin,cyrillic' ],
        '<!--[if IE]>',
            [ elem=> 'css', url=> '/desktop.bundles/index/_index.ie.css' ],
        '<![endif]-->',
        '<!--[if IE 9]>',
            [ elem=> 'css', url=> '/desktop.bundles/index/_index.ie9.css' ],
        '<![endif]-->'
    ],
    scripts=> [
        [ elem=> 'js', url=> '/desktop.bundles/index/_index.js' ],
    ],
    content=> [
      [
        block=> "section",
        "name"=> "header",
        content=> [
          [
            block=> "row",
            mods=> [svam=> true],
            content=> [
              [
                elem=> 'col',
                mods=> [mw=> 4],
                content=> [
                  [
                    block=> 'logo',
                    'url' => $content['logo']['url'],
                    'name' => $content['logo']['name'],
                    'desc' => $content['logo']['desc'],
                  ]
                ]
              ],
              [
                elem=> 'col',
                mods=> [mw=> 4],
                content=> [
                  [
                    block=> 'sitename',
                    content=> "ИСПАНИЯ из Краснодара"
                  ]
                ]
              ],
              [
                elem=> 'col',
                mods=> [mw=> 4],
                content=> [
                  [
                    block=> 'callme',
                    text=> 'ЗВОНИ!',
                    phone=> $content['phones'][0]
                  ],
                  //[
                    //block=> 'menu',
                    //mods=> [collapseable=> true],
                    //content=> []
                  //]
                ]
              ],
            ]
          ],
          [
            block=> 'ordertop',
            content=> [
              [
                block=> "button",
                mods=> [type=>"link", theme=> "white", size=> "l", action=> "lightbox"],
                mix=> [[block=>"ordertop", elem=>'button']],
                url=> Form::get(1)->id(),
                text=> "Заказать тур", 
              ],
            ]
          ]
        ]
      ], //section header
      [
        block=> "slider",
        js=> [items=> 1, center=> true, autoplay=> false, 'dots' => false],
        mods=> [type=> 'owl', 'pos' => 'header', width=> 'full', height=>'fullscreen'],
        content=> [
          [
            block=> 'slide',
            mods=> [width=> 'full'],
            url=> '/img/slide_green.jpg',
            content=> [
              [
                block=> 'reserv',
                'url' => Form::get(1)->id(),
                content=> [
                  [
                    'elem'=> 'head',
                    content=> "Раннее бронирование"
                  ],
                  [
                    elem=> 'content',
                    content=> [
                      [
                        elem=> 'sale',
                        content=> [
                          [
                            elem=> 'sale-count',
                            content=> '-35%'
                          ],
                          [
                            elem=> 'sale-text',
                            content=> 'скидка'
                          ],
                        ]
                      ],
                      [
                        elem=> 'text',
                        content=> [
                          [
                            block=> 'stars',
                            count=> 3,
                            max=> 5, 
                          ],
                          [
                            elem=> 'hotel',
                            content=> 'Armonia'
                          ],
                          [
                            elem=> 'comment',
                            content=> 'Отдых для двоих'
                          ],
                          [
                            elem=> 'prices',
                            content=> [
                              [
                                block=> 'price',
                                mods=> [actual=> 'old'],
                                count=> '95 290',
                                curr=> 'руб.'
                              ],
                              [
                                block=> 'price',
                                mods=> [actual=> 'new'],
                                count=> '73 290',
                                from=> 'от',
                                curr=> 'руб.'
                              ],
                            ]
                          ]
                        ]
                      ]
                    ]
                  ]
                ]
              ]
            ]
          ],
        ]
      ],
      [
        block=> "section",
        name=> "best",
        head=> "Лучшие курорты Испании",
        content=> [
          [
            block=>'resorts',
            mix=> [
              block=> 'row',
              mods=> [svat=> true, mac=> true],
            ],
            content=> [
              array_map(function($resort){
                return [
                    block=> "resort",
                    mix=> [block=> 'row', elem=> 'col', mods=> [mw=> 6]],
                    name=> $resort['name'],
                    image=> $resort['image'],
                    text=>  $resort['text'],
                    content=> [
                      [
                        block=> "resort-info",
                        'url' => Form::get(1)->id(),
                        content=> [
                          [
                            elem=> 'tag',
                            content=> $resort['tag'],
                          ],
                          [
                            block=> "stars",
                            count=> $resort['stars'],
                            mix=> [block=> "resort-info", elem=> "stars"], 
                          ],
                          [
                            elem=> 'hotel-name',
                            content=> $resort['hotel']
                          ],
                          [
                            elem=> 'time',
                            content=> $resort['time'] 
                          ],
                          [
                            elem=> 'price',
                            count=> $resort['price'],
                            curr=> "руб./ чел."
                          ]
                        ]
                      ],
                    ]
                ];
              }, $content['resorts']),
            ]
          ],
          //[
            //block=> 'button',
            //mods=> [theme=> 'white', size=> 'l'],
            //text=> 'Все туры'
          //]
        ]
      ], //section best
      [
        block=> 'section',
        name=> 'we-know',
        content=> [
          [
            block=> 'row',
            content=> [
              [
                block=> 'we-know-all',
                mix=> [block=> 'row', elem=> 'col', mods=> [mw=>4, mo=> 7]],
                content=> [
                  [
                    elem=> 'head',
                    content=> 'Мы знаем об Испании <br> абсолютно ВСЁ!', 
                  ],
                  [
                    elem=> 'item',
                    content=>'Индивидуальный подход',
                  ],
                  [
                    elem=> 'item',
                    content=> 'Мы знаем точное расписание многочисленных карнавалов и веселых ярмарок'
                  ],
                  [
                    elem=> 'item',
                    content=> 'Прямые вылеты из Краснодара 2 раза в неделю и ежедневно из Ростова, Минвод или Москвы'
                  ],
                  [
                    elem=> 'item',
                    content=>'возможность выезда менеджера в офис или домой',
                  ],
                ]
              ], //we know all
            ]
          ],
        ]
      ], //section we know
      [
        block=>'container',
        content=> [
          [
            block=> 'diffs',
            content=> [
              [
                block=> 'diff',
                icon=> 'photo',
                content=> '12 рускоязычных фотографов готовы к фотосессии'
              ],
              [
                block=> 'diff',
                icon=> 'moto',
                content=> 'Аренда автомобилей или мопедов более чем в 70 компаниях',
              ],
              [
                block=> 'diff',
                icon=> 'beach',
                content=> 'Только мы знаем о всех 73 пляжах Испании'
              ],
              [
                block=> 'diff',
                icon=> 'meal',
                content=> 'Отели по системе все включено'
              ],
              [
                block=> 'diff',
                icon=> 'man',
                content=> '27 рускоязычных гидов по всей стране',
              ],
              [
                block=> 'diff',
                icon=> 'road',
                content=> 'Мы знаем каждую из 168 экскурсий'
              ],
            ]
          ]
        ]
      ],
      [
        block=> 'section',
        name=> 'best',
        content=> [
          [
            block=> 'heading',
            lvl=> 3,
            content=> 'Только лучшие пляжи Испании'
          ],
          /* beaches */
          [
            block=> 'beaches',
            content=> 
                    array_map( function($b)
                    {
                      return
                        [
                          block=> 'beach',
                          name=> $b['name'],
                          price=> 'от '.$b['price'].' руб.',
                          image=> $b['img'],
                        ];
                    }, $content['beaches']),
          ],
        ]
      ], //section beaches
      [
        block=> 'port-aventura',
        content=> [
          [
            block=> 'heading',
            lvl=> 2,
            content=> 'Порт Авентура', 
          ],
          [
            block=> 'section',
            mods=> [name=> 'port-aventura'],
            background=> '/img/aventura.jpg',
            content=> [
              [
                block=> 'row',
                content=> [
                  [
                    elem=> 'col',
                    mix=> [block=> 'port-aventura', elem=> 'col'],
                    mods=> [mw=> 6],
                    content=> [
                      [
                        block=> 'article',
                        mods => ['cs' => 'inverse'],
                        content=> [
                          '<p>Парк аттракционов «Порт Авентура» (PortAventura), расположен в Салоу на побережье Коста-Дорада.</p>',
                          '<p>Парк разделён на шесть тематических зон, каждая из которых представляет одну из исторических цивилизаций, достоверно воспроизводя характерные каждой культуре черты. </p><p>Все это способствует полному погружению в другие «миры». С 1995 по 2010 год в парке было 5 зон, в 2011 году открылась «Сезам Авентура» (детская зона).</p>',
                          [
                            block=> 'port-aventura',
                            elem=> 'button',
                            content=> [
                              [
                                block=> 'button',
                                mods=> [action => 'lightbox', size=> 'l', theme=> 'white'],
                                url=> Form::get(1)->id(),
                                text=> 'Узнать больше'
                              ],
                            ]
                          ],
                        ]
                      ],
                      [
                        block=> 'adv',
                        mix=> [ block=> 'port-aventura', elem=> 'adv'],
                        content=> [
                          [
                            elem=> 'text',
                            content=> 'Позвони нам и узнай, как посещать парк аттракционов БЕСПЛАТНО<br> на протяжении всего отдыха '
                          ],
                          [
                            block=> 'phone',
                            mods=> [size=> 'xl'],
                            content=> '8 (989) 123-123-1'
                          ]
                        ]
                      ]
                    ]
                  ], //col
                  [
                    elem=> 'col',
                    mods=> [mw=> 6],
                    mix=> [block=> 'port-aventura', elem=> 'col'],
                    content=> [
                      [
                        block=> 'slider',
                        js=> [items=> 1, center=> true],
                        mods=> [type=> 'owl', pos=> 'aventura'],
                        content=> array_map(function($b){
                            return
                              [
                                block=> 'slide',
                                mods => ['hasdesc'=>true],
                                mix => [block => 'lightbox', js => [url => Form::get(1)->id()]],
                                content=> [
                                  [
                                    block=> 'image',
                                    url=> $b['img'],
                                    mix=> [block=> 'slide', elem=> 'image']
                                  ],
                                  [
                                    elem=> 'description',
                                    content=> $b['desc']
                                  ]
                                ]
                              ];
                          }, $content['av-sliders'][0]['slides']) 
                      ],
                      [
                        block=> 'slider',
                        js=> [items=> 1, center=> true],
                        mods=> [type=> 'owl', pos=> 'aventura'],
                        content=> array_map(function($b){
                            return
                              [
                                block=> 'slide',
                                mods => ['hasdesc'=>true],
                                mix => [block => 'lightbox', js => [url => Form::get(1)->id()]],
                                content=> [
                                  [
                                    block=> 'image',
                                    url=> $b['img'],
                                    mix=> [block=> 'slide', elem=> 'image']
                                  ],
                                  [
                                    elem=> 'description',
                                    content=> $b['desc']
                                  ]
                                ]
                              ];
                          }, $content['av-sliders'][1]['slides']) 
                      ],
                    ]
                  ], //col
                ]
              ] //row
            ]
          ], //section port 
        ]
      ], //port aventura
      [
        block=> "section",
        name=> 'all-inclusive',
        head=> 'Лучшие предложения All inclusive',
        content=> array_map(function($b){
          return
            [
              block=> 'inclusive',
              name=>  $b['name'],
              price=> 'от '.$b['price']."&nbsp;руб.",
              image=> $b['img'],
              button=> 'Узнать больше',
              url=> Form::get(1)->id(),
            ];
        }, $content['all-inclusive'])
      ], //all inclusive
      [
        block=> 'port-aventura',
        content=> [
          [
            block=> 'heading',
            lvl=> 2,
            content=> 'Экскурсионная Испания', 
          ],
          [
            block=> 'section',
            mods=> [name=> 'port-aventura', 'cs' => 'white'],
            background=> '/img/exc-3.jpg',
            content=> [
              [
                block=> 'row',
                content=> [
                  [
                    elem=> 'col',
                    mix=> [block=> 'port-aventura', elem=> 'col'],
                    mods=> [mw=> 6],
                    content=> [
                      [
                        block=> 'article',
                        content=> '<p>Долгие песчаные пляжи, симпатичные города, живописные горы, примечательное архитектурное наследие вкупе с развитым виноделием и лучшими традициями гостеприимства – всё это участок побережья Средиземного моря, начиная от Барселоны и заканчивая Коста Брава.</p>'
                      ],
                      [
                        block=> 'port-aventura', elem=> 'button',
                        content=> [
                          [
                            block=> 'button',
                            mods=> [action=> 'lightbox', size=> 'l', theme=> 'white'],
                            url=> Form::get(1)->id(),
                            text=> 'Узнать больше'
                          ],
                        ]
                      ],
                    ]
                  ], //col
                  [
                    elem=> 'col',
                    mods=> [mw=> 6],
                    mix=> [block=> 'port-aventura', elem=> 'col'],
                    content=> [
                      [
                        block=> 'slider',
                        js=> [items=> 1, center=> true],
                        mods=> [type=> 'owl', pos=> 'aventura'],
                        content=> array_map(function($b){
                            return
                              [
                                block=> 'slide',
                                mods => ['hasdesc'=>true],
                                mix => [block => 'lightbox', js => [url => Form::get(1)->id()]],
                                content=> [
                                  [
                                    block=> 'image',
                                    url=> $b['img'],
                                    mix=> [block=> 'slide', elem=> 'image']
                                  ],
                                  [
                                    elem=> 'description',
                                    content=> $b['desc']
                                  ]
                                ]
                              ];
                          }, $content['sp-sliders'][0]['slides']) 
                      ],
                      [
                        block=> 'slider',
                        js=> [items=> 1, center=> true],
                        mods=> [type=> 'owl', pos=> 'aventura'],
                        content=> array_map(function($b){
                            return
                              [
                                block=> 'slide',
                                mods => ['hasdesc'=>true],
                                mix => [block => 'lightbox', js => [url => Form::get(1)->id()]],
                                content=> [
                                  [
                                    block=> 'image',
                                    url=> $b['img'],
                                    mix=> [block=> 'slide', elem=> 'image']
                                  ],
                                  [
                                    elem=> 'description',
                                    content=> $b['desc']
                                  ]
                                ]
                              ];
                          }, $content['sp-sliders'][1]['slides']) 
                      ]
                    ]
                  ], //col
                ]
              ] //row
            ]
          ], //section port 
        ]
      ], //port aventura
      //[
        //block=>'section',
        //name=> 'tours-filter',
        //head=> 'Туры в Испанию',
        //content=> [
          //[
            //block=> 'tours-filter',
            //content=> []
          //]
        //]
      //], //filter
      [
        block=> 'section',
        name=> 'partners',
        head=> $content['partners']['head'],
        content=> [
          [
            block=> 'partners',
            partners=> $content['partners']['tours']
          ]
        ]
      ], //section partners
      [
        block=> 'section',
        name=> 'counts',
        content=> [
          [
            block=> 'counts',
            content=> array_map(function($b){
              return
                [
                  block=> 'count',
                  count=> $b['count'],
                  text => $b['text'],
                ];
            }, $content['counts'])
          ]
        ]
      ], //counts
      [
        block=> 'section',
        name=> 'order-now',
        content=> [
          [
            block=> 'row',
            mods=> [svam=> true, sac=> true],
            content=> [
              [
                 elem=> 'col',
                 mods=> [mw=> 4],
                 content=> [
                    [
                      block=> 'order-now',
                      content=> [
                        [
                          elem => 'order',
                          content=>'Закажи сейчас',
                          mix => [
                            block => 'lightbox',
                            js => ['url' => Form::get(1)->id()]
                          ]
                        ],
                        [tag=> 'small', content=>'и такси до аэропорта'],
                        [elem=> 'free', content=> 'Бесплатно'] 
                      ]
                    ]
                 ]
              ], //col
              [
                elem=> 'col',
                mods=> [mw=> 4],
                content=> [
                  [
                    block=> 'counter',
                    text=> $content['action']['counter']['pre'],
                  ]
                ]
              ], //col
              [
                elem=> 'col',
                mods=> [mw=> 4],
                content=> [
                  [
                    block=> 'taxi'
                  ]
                ]
              ]
            ]
          ]
        ]
      ], //order-now
      [
        block=> "section",
        "name"=> "bottom-contact",
        content=> [
          [
            block=> "row",
            mods=> [svam=> true],
            content=> [
              [
                elem=> 'col',
                mods=> [mw=> 4],
                content=> [
                  [
                    block=> 'logo',
                    'url' => $content['logo']['url'],
                    'name' => $content['logo']['name'],
                    'desc' => $content['logo']['desc'],
                  ]
                ]
              ],
              [
                elem=> 'col',
                mods=> [mw=> 4, sol=> true],
                content=> [
                  [
                    block=> 'contact-info',
                    content=> "Контактная информация"
                  ]
                ]
              ],
              [
                elem=> 'col',
                mods=> [mw=> 4],
                content=> [
                  [
                    block=> 'callme',
                    text=> 'ЗВОНИ!',
                    phone=> $content['phones'][0]
                  ],
                ]
              ],
            ]
          ],
        ]
      ], //section header
      [
        block=> 'section',
        name=> 'contacts',
        content=> [
          [
            block=> 'row',
            content=> [
              [
                elem=> 'col',
                mix=> [block=> 'contacts-col'],
                mods=> [mw=> 6],
                content=> [
                  [
                    block=> 'map',
                    mods=> [vendor=> 'yandex', size=> 'm'],
                    center=> $content['map'],
                    'mark'=>$content['map'],
                    zoom=> 13
                  ],
                  [
                    block=> 'contacts-wrap',
                    mix=> [block=> 'row'],
                    content=> [
                      [
                        elem=> 'col',
                        mods=> [mw=> 6],
                        content=> [
                          [
                            block=> 'contacts',
                            content=> [
                              [
                                elem=> 'label',
                                content=> 'Телефоны'
                              ],
                              array_map(function($b){
                                return
                                  [
                                    block=> 'contact',
                                    mods=> [type=> 'phone'],
                                    content=> $b
                                  ];
                              }, $content['phones'])
                            ]
                          ]
                        ]
                      ],
                      [
                        elem=> "col",
                        mods=> [mw=> 6],
                        content=> [
                          [
                            block=> 'contacts',
                            content=> [
                              [
                                elem=> 'label',
                                content=> 'Адрес в Краснодаре'
                              ],
                              [
                                block=> 'contact',
                                mods=> [type=> 'address'],
                                content=> $content['address'] 
                              ],
                              [
                                block=> 'contact',
                                mods=> [type=> 'email'],
                                label=> 'E-mail:',
                                content=> $content['email'] 
                              ],
                            ]
                          ] //contacts 
                        ]
                      ]
                    ]
                  ], //contacts
                ]
              ], //col
              [
                elem=> 'col',
                mods=> [mw=> 6],
                mix=> [block=> 'contacts-col', mods=>[border=> true]],
                content=> [
                  Form::get(2)->bem()
                ]
              ]
            ]
          ],
        ]
      ],//section contacts
      [
        block=> 'section',
        name=> 'footer',
        content=> [
          [
            block=> "row",
            mods=> [svam=>true],
            content=> [
              [
                elem=> 'col',
                mods=> [mw=> 6],
                content=> [
                  [
                    block=> 'copyright',
                    content=> [
                    'Агентство путешествий "Оранжевое море", Все права защищены. 2014 ',
                    'Дизайн сайта: ',
                      [
                        block => 'link',
                        content => 'www.diz.alla4u.ru',
                        'url' => '//www.diz.alla4u.ru'
                      ]
                    ]
                  ]
                ]
              ],
              [
                elem=> 'col',
                mods=> [mw=> 6, sof=> true],
                content=> [
                  [
                    block=> 'social-share',
                    content=> [
                      'Поделитесь ссылкой:&thinsp;' ,
                      [
                        block=> 'social',
                        links=> array_map(function($u ,$n){
                            return [name=> $n, url=> $u];
                          }, $content['social'], array_keys($content['social']))
                      ]
                    ]
                  ],
                ]
              ]
            ]
          ]
        ]
      ], //footer
      [
        block => 'hidden',
        content => [
          Form::get_hidden_bem() 
        ]
      ], //hidden
      [
        block=> 'link',
        url => 'tel:'.$content['phones'][0],
        content => [
          [
            block => 'float-phone',
            mods => ['theme'=>'tez', 'pos'=>'bottom-right', 'visible' => 'phone'],
          ]
        ]
      ]
    ]

); //end tempate
